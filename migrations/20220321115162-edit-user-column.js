"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "contacts",
          "firstName",
          {
            type: Sequelize.DataTypes.STRING,
          },
          { transaction: t }
        ),

        queryInterface.addColumn(
          "contacts",
          "lastName",
          {
            type: Sequelize.DataTypes.STRING,
          },
          { transaction: t }
        ),

        queryInterface.addColumn(
          "contacts",
          "company",
          {
            type: Sequelize.DataTypes.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("contacts", "firstName", {
          transaction: t,
        }),
        queryInterface.removeColumn("contacts", "lastName", { transaction: t }),
        queryInterface.removeColumn("contacts", "company", { transaction: t }),
      ]);
    });
  },
};
