import Express, { response } from "express";
import { Op } from "sequelize";
import { json } from "sequelize/types";
import bcrypt from "bcryptjs";
import {
  UserAttributes,
  UserModel,
} from "./shared/sequelize/models/user.model";
import jwt from "jsonwebtoken";
import { TokenModel } from "./shared/sequelize/models/token.model";
import dotenv, { parse } from "dotenv";
dotenv.config();
let app = Express();
app.use(Express.json());
import express, { Response } from "express";

export interface Request extends express.Request {
  user: UserModel;
}

export async function authenticationRequest(req: any, res: any, next: any) {
  try {
    let accessToken = req.headers.authorization; // Bearer afasdfasdfasfasfddf
    accessToken = accessToken.split(" ");
    accessToken = accessToken.length > 0 ? accessToken[1] : undefined;

    if (!accessToken) {
      res.sendStatus(401);
      return;
    }

    let payload: UserAttributes = jwt.verify(
      accessToken,
      process.env.ACCESS_TOKEN_KEY as any
    ) as any;

    if (!payload) {
      res.sendStatus(403);
      return;
    }

    let dbAccessToken = await TokenModel.findOne({
      where: { userId: payload.id, type: TokenType.ACCESS },
    });

    if (!dbAccessToken) {
      res.sendStatus(403).send("Session expired or user has logged out.");
      return;
    }

    let user = await UserModel.findOne({ where: { id: payload.id } });

    if (!user) {
      res.sendStatus(403).send("User does not exist anymore.");
      res.sendStatus(403).send("User does not exist anymore.");
      return;
    }

    req.user = user;
    next();
  } catch (e: any) {
    res.sendStatus(401);
    return;
  }
}
export function authEndPoints(app: any) {
  app.get("/logout", authenticationRequest, async (req: Request, res: any) => {
    let tokens = await TokenModel.findAll({
      where: {
        userId: req.user.get("id"),
        type: {
          [Op.in]: [TokenType.ACCESS, TokenType.REFRESH],
        },
      },
    });

    if (tokens.length > 0) await removeAllTokens(tokens);

    res.send({});
  });
  function removeAllTokens(tokens: TokenModel[]): Promise<any> {
    return Promise.all(
      tokens.map((token) => {
        return TokenModel.destroy({ where: { id: token.get("id") } });
      })
    );
  }
  app.post(
    "/login",
    async (req: { body: { email: string; password: string } }, res: any) => {
      let user = await UserModel.findOne({
        where: { email: req.body.email },
      });
      if (!user) {
        res.sendStatus(404);
        return;
      }
      let isPasswordValid = bcrypt.compareSync(
        req.body.password,
        user.get("password")
      );

      if (!isPasswordValid) {
        res.sendStatus(401);
        return;
      }
      let payload: Omit<UserAttributes, "password"> & { password?: string } =
        JSON.parse(JSON.stringify(user));

      let accessToken = generateAccessToken(payload);
      let refreshToken;
      let userRefrshTokens = await TokenModel.findAll({
        where: { userId: user.get("id"), type: "refresh" },
      });
      let dbAccessTokens = await TokenModel.findAll({
        where: { userId: user.get("id"), type: TokenType.ACCESS },
      });
      if (dbAccessTokens) {
        await removeAllTokens(dbAccessTokens);
      }

      await TokenModel.create({
        type: TokenType.ACCESS,
        userId: payload.id!,
        token: accessToken,
      });
      if (userRefrshTokens.length < 1) {
        refreshToken = generateRefreshToken(payload);

        await TokenModel.create({
          type: "refresh",
          userId: user.get("id"),
          token: refreshToken,
        });
      } else {
        refreshToken = userRefrshTokens[0].get("token");
      }
      delete payload.password;

      res.send({ user: payload, accessToken, refreshToken });
    }
  );
  app.get("/logout", authenticationRequest, async (req: Request, res: any) => {
    let tokens = await TokenModel.findAll({
      where: {
        userId: req.user.get("id"),
        type: {
          [Op.in]: [TokenType.ACCESS, TokenType.REFRESH],
        },
      },
    });

    if (tokens.length > 0) await removeAllTokens(tokens);

    res.send({});
  });
  app.get("/token", async (req: any, res: any) => {
    let refreshToken: any = req.headers.authorization;
    refreshToken = refreshToken.split(" ");
    refreshToken = refreshToken.length > 0 ? refreshToken[1] : undefined;

    if (!refreshToken) {
      res.sendStatus(401);
      return;
    }

    try {
      let user: any = jwt.verify(
        refreshToken,
        process.env.REFRESH_TOKEN_KEY as any
      );

      if (user) {
        user = await UserModel.findOne({ where: { id: user.id } });

        if (!user) {
          res.sendStatus(403).send("User does not exist anymore.");
          return;
        }

        let accessToken = generateAccessToken(JSON.parse(JSON.stringify(user)));

        res.send({ accessToken });
      } else {
        res.sendStatus(403);
      }
    } catch (e: any) {
      res.sendStatus(403);
    }
  });
  function generateAccessToken(user: any): string {
    return jwt.sign(user, process.env.ACCESS_TOKEN_KEY as any, {
      expiresIn: "15m",
    });
  }
  function generateRefreshToken(user: any): string {
    return jwt.sign(user, process.env.REFRESH_TOKEN_KEY as any);
  }
}
export enum TokenType {
  ACCESS = "access",
  REFRESH = "refresh",
}
