import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../connection";

export interface UserAttributes {
  id?: number;
  name: string;
  email: string;
  password: string;
  readonly createdAt?: string;
  updatedAt?: string;
}

export interface UserCreationAttributes
  extends Optional<UserAttributes, "id" | "updatedAt" | "createdAt"> {}

export class UserModel
  extends Model<UserAttributes, UserCreationAttributes>
  implements UserAttributes
{
  id: number;
  name: string;
  email: string;
  password: string;
  readonly createdAt?: string;
  updatedAt?: string;
}

UserModel.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: new DataTypes.STRING(),
      unique: true,
    },
    name: {
      type: new DataTypes.STRING(),
    },
    password: {
      type: new DataTypes.STRING(),
    },
    createdAt: {
      type: new DataTypes.DATE(),
    },
    updatedAt: {
      type: new DataTypes.DATE(),
    },
  },
  {
    tableName: "users",
    sequelize: db,
  }
);
