import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../connection";

export interface ContactAttributes {
  id?: number;
  firstName: string;
  lastName: string;
  company: string;
  email: string;
  phone: string;
  pic: string;
  userId: number;
  readonly createdAt?: string;
  updatedAt?: string;
}

export interface ContactCreationAttributes
  extends Optional<ContactAttributes, "id" | "updatedAt" | "createdAt"> {}

export class ContactModel
  extends Model<ContactAttributes, ContactCreationAttributes>
  implements ContactAttributes
{
  id: number;
  firstName: string;
  lastName: string;
  company: string;
  email: string;
  phone: string;
  pic: string;
  userId: number;
  readonly createdAt?: string;
  updatedAt?: string;
}

ContactModel.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: new DataTypes.STRING(),
      unique: true,
    },
    firstName: {
      type: new DataTypes.STRING(),
    },
    lastName: {
      type: new DataTypes.STRING(),
    },
    pic: {
      type: new DataTypes.STRING(),
    },
    company: {
      type: new DataTypes.STRING(),
    },
    userId: {
      type: new DataTypes.INTEGER(),
    },
    phone: {
      type: new DataTypes.STRING(),
    },
    createdAt: {
      type: new DataTypes.DATE(),
    },
    updatedAt: {
      type: new DataTypes.DATE(),
    },
  },
  {
    tableName: "contacts",
    sequelize: db,
  }
);
