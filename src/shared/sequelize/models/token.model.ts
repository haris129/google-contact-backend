import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../connection";

export interface TokenAttributes {
  id?: number;
  token: string;
  type: string;
  userId: number;
  readonly createdAt?: string;
  updatedAt?: string;
}

export interface TokenCreationAttributes
  extends Optional<TokenAttributes, "id" | "updatedAt" | "createdAt"> {}

export class TokenModel
  extends Model<TokenAttributes, TokenCreationAttributes>
  implements TokenAttributes
{
  id: number;
  token: string;
  type: string;
  userId: number;
  readonly createdAt?: string;
  updatedAt?: string;
}

TokenModel.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataTypes.INTEGER,
    },
    token: {
      type: new DataTypes.STRING(),
    },
    type: {
      type: new DataTypes.STRING(),
    },
    createdAt: {
      type: new DataTypes.DATE(),
    },
    updatedAt: {
      type: new DataTypes.DATE(),
    },
  },
  {
    tableName: "tokens",
    sequelize: db,
  }
);
