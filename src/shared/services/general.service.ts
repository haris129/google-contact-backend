import * as fs from "fs";
import { v4 as uuidv4 } from "uuid";

export class GeneralService {
  static uploadFile(base64: string): string {
    let mimeInfo = base64.substring(
      "data:image/".length,
      base64.indexOf(";base64")
    );
    if (!fs.existsSync("./uploads")) {
      fs.mkdirSync("./uploads");
    }
    let fileName = uuidv4() + "." + mimeInfo;

    var imageBuffer = decodeBase64Image(base64);
    fs.writeFile("./uploads/" + fileName, imageBuffer.data, function (err) {});

    return fileName;
  }
}

function decodeBase64Image(dataString: string) {
  var matches: any = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response: any = {};

  if (matches.length !== 3) {
    return new Error("Invalid input string");
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], "base64");

  return response;
}
