import Express from "express";
import { Op } from "sequelize";
import { UserModel } from "./shared/sequelize/models/user.model";
import jwt from "jsonwebtoken";
import { TokenModel } from "./shared/sequelize/models/token.model";
import dotenv, { parse } from "dotenv";
import { authEndPoints, authenticationRequest } from "./auth";
import cors from "cors";
import * as fs from "fs";
import { v4 as uuidv4 } from "uuid";

dotenv.config();
let app = Express();
app.use(cors());
app.use(Express.json({ limit: "10mb" }));
authEndPoints(app);

app.listen(3100, () => {
  console.log("listening on 3100");
});

import {
  ContactAttributes,
  ContactModel,
} from "../src/shared/sequelize/models/contact.model";
import { GeneralService } from "./shared/services/general.service";
import { Json } from "sequelize/types/utils";
interface Request extends Express.Request {
  user: UserModel;
}
interface ContactRequest extends Omit<Request, "body"> {
  body: {
    contact: ContactAttributes;
  };
}

app.get(
  "/contacts",
  authenticationRequest,
  async (req: Request & { query: { page: string } }, res: any) => {
    console.log(req.query);

    let conditions = {
      userId: req.user.get("id"),
    };
    let totalContacts = await ContactModel.count({ where: conditions });
    let limit = 2;
    let currentPage: any = parseInt(req.query.page);
    let startIndex = (currentPage - 1) * limit;

    let contacts = await ContactModel.findAll({
      where: conditions,
      limit,
      offset: startIndex,
      order: [["firstName", "ASC"]],
    });

    res.send({ total: totalContacts, data: contacts });
  }
);

app.get(
  "/contacts/:searchKeyword",
  authenticationRequest,
  async (req: Request, res: any) => {
    let contacts = await ContactModel.findAll({
      where: {
        userId: req.user.get("id"),
        [Op.or]: [
          {
            firstName: {
              [Op.like]: `%${req.params.searchKeyword}%`,
            },
          },
          {
            lastName: {
              [Op.like]: `%${req.params.searchKeyword}%`,
            },
          },
        ],
      },
    });

    res.send(contacts);
  }
);
app.delete(
  "/contacts/:id",
  authenticationRequest,
  async (req: Request, res: any) => {
    if (!req.params.id) {
      res.sendStatus(404);
      return;
    }

    let contact = await ContactModel.findOne({
      where: {
        id: req.params.id,
      },
    });

    if (!contact) {
      res.sendStatus(404);
      return;
    }

    if (contact.get("userId") !== req.user.get("id")) {
      res.sendStatus(404);
      return;
    }

    await contact.destroy();

    res.send({});
  }
);

app.put(
  "/contacts/:id",
  authenticationRequest,
  async (req: Request, res: any) => {
    if (!req.params.id) {
      res.sendStatus(404);
      return;
    }

    if (!req.body.contact) {
      res.sendStatus(404);
      return;
    }

    // Check if user has sent request without providing values to update
    let values: string[] = [];
    Object.keys(req.body.contact).forEach((key: any) => {
      // @ts-ignore
      let value: any = req.body.contact[key];
      if (["firstName", "lastName", "email", "phone"].includes(key))
        values.push(value);
    });

    if (values.length < 1) {
      res.sendStatus(404);
      return;
    }

    let contact = await ContactModel.findOne({
      where: {
        id: req.params.id,
      },
    });
    console.log(req.params.id);

    if (!contact) {
      res.sendStatus(404);
      return;
    }

    if (contact.get("userId") !== req.user.get("id")) {
      res.sendStatus(404);
      return;
    }
    let text = req.body.contact.pic;

    const myArray = text.split("file/");
    console.log(contact?.get("pic"));
    console.log("hello" + myArray[1]);
    console.log(req.body.contact.pic);

    /*
    if (req.body.contact.pic != contact.pic) {
      let fileName = GeneralService.uploadFile(req.body.contact.pic);
      req.body.contact.pic = fileName;
    }*/

    if (
      req.body.contact.pic ==
      "https://ssl.gstatic.com/s2/oz/images/sge/grey_silhouette.png"
    ) {
      delete req.body.contact.pic;
    } else {
      if (!myArray[1]) {
        let fileName = GeneralService.uploadFile(req.body.contact.pic);
        req.body.contact.pic = fileName;
      } else {
        let pic = req.body.contact.pic.split("file/");
        req.body.contact.pic = pic[1];
      }
    }

    await contact.update(req.body.contact);

    res.send(contact);
  }
);

interface CreateContactsRequest extends Omit<Request, "body"> {
  body: {
    contact: ContactAttributes;
  };
}
app.get("/file/:fileName", (req, resp) => {
  resp.setHeader("content-type", "image/jpeg");
  fs.createReadStream("./uploads/" + req.params.fileName)
    .on("error", (err) => {
      console.log(err);
    })
    .pipe(resp);
});
app.get(
  "/contacts/single/:id",
  authenticationRequest,
  async (req: Request, res: any) => {
    console.log("req.params.id", req.params.id);
    let conditions = {
      userId: req.user.get("id"),
      id: req.params.id,
    };

    let contact = await ContactModel.findOne({
      where: conditions,
    });

    res.send(contact); // {total: 40, data:[{id: 1, name: abc}]}
  }
);
app.post(
  "/contacts",
  authenticationRequest,
  async (req: CreateContactsRequest, res: any) => {
    console.log(req);
    if (!req.body.contact) {
      res.sendStatus(401);
      return;
    }
    console.log(req.body.contact.pic);
    Object.keys(req.body.contact).forEach((key: string) => {
      if (
        !["firstName", "lastName", "email", "company", "phone", "pic"].includes(
          key
        )
      )
        // @ts-ignore

        delete req.body.contact[key];
    });

    if (req.body.contact.pic) {
      let fileName = GeneralService.uploadFile(req.body.contact.pic);
      req.body.contact.pic = fileName;
    }

    let contact = await ContactModel.create({
      ...req.body.contact,
      userId: req.user.get("id"),
    });

    res.send(contact);
  }
);
